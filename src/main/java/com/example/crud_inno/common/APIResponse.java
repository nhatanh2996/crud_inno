package com.example.crud_inno.common;

import java.io.Serializable;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class APIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int codeStatus;
	private String messageStatus;
	private String description;
	private long took; // duration time
	private Object data;

	public APIResponse(int codeStatus, String messageStatus, String description, long took, Object data) {
		super();
		this.codeStatus = codeStatus;
		this.messageStatus = messageStatus;
		this.description = description;
		this.took = took;
		this.data = data;
	}

	public int getCodeStatus() {
		return codeStatus;
	}

	public void setCodeStatus(int codeStatus) {
		this.codeStatus = codeStatus;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getTook() {
		return took;
	}

	public void setTook(long took) {
		this.took = took;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
