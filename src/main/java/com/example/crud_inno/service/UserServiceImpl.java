package com.example.crud_inno.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.crud_inno.dao.UserDao;
import com.example.crud_inno.dao.UserPaginationDao;
import com.example.crud_inno.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	@Autowired
	private UserPaginationDao userPaginationDao;

	@Override
	public List<User> getAllUser() {
		return (List<User>) userDao.findAll();
	}

	@Override
	public User addUser(User user) {
		if (null == user || user.getName().isEmpty() || user.getEmail().isEmpty() || user.getAddress().isEmpty()
				|| user.getBirthday().isEmpty()) {
			return null;
		}
		return userDao.save(user);
	}

	@Override
	public User updateUser(User user) {
		if (null == user)
			return null;

		Optional<User> oldUser = userDao.findById(user.getUserId());
		if (!oldUser.isPresent()) { // check null 
			return null;
		}
		return addUser(user);
	}

	@Override
	public void deleteUser(Integer userId) {		
		userDao.deleteById(userId);
	}

	@Override
	public List<User> getAllUserWithOldLessThan(Integer old) {
		
		return userDao.findAllUserWithOldLessThan(old);
	}

	@Override
	public Page<User>  getUserWithPaging(Pageable pageable) {
		
		return  userPaginationDao.findAll(pageable);
	}

}
