package com.example.crud_inno.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.crud_inno.model.User;

public interface UserService {
	List<User> getAllUser();
	
	List<User> getAllUserWithOldLessThan(Integer old);
	
	Page<User> getUserWithPaging(Pageable pageable);
	
	User addUser(User user);

	User updateUser(User user);
	
	void deleteUser (Integer userId);
	
}
