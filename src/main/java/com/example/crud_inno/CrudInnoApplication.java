package com.example.crud_inno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudInnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudInnoApplication.class, args);
	}

}
