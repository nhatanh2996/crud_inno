package com.example.crud_inno.dao;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.crud_inno.model.User;

@Repository
public interface UserDao extends CrudRepository<User, Integer>{
	

	@Query(value = "SELECT * FROM user AS u WHERE YEAR(CURDATE()) - YEAR(u.birthday) < :old", nativeQuery = true)
	List<User> findAllUserWithOldLessThan(@Param("old") Integer old);
	
}
