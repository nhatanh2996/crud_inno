package com.example.crud_inno.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.crud_inno.model.User;

@Repository
public interface UserPaginationDao extends JpaRepository<User, Integer>{
	
}
