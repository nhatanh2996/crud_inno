package com.example.crud_inno.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.crud_inno.model.User;
import com.example.crud_inno.service.UserService;

@RestController
public class UserController {
	
	private static  final String UPLOADED_FOLDER = "C:\\Image\\";
	
	private static final String STATIC_CONTEXT_PATH = "localhost:8080/images/";
	
	@Autowired
	private UserService userService;

	@GetMapping(value = "/")
	public String homepage() {
		return "index";
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public List<User> getAllUser() {
		return userService.getAllUser();
	}

	@RequestMapping(value = "/getAllUserWithOld", method = RequestMethod.GET)
	public List<User> getAllUserWithOld(@RequestParam(value = "old", required = true) int old) {
		return userService.getAllUserWithOldLessThan(new Integer(old));
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public User addUser(User user, @RequestParam(value = "fileImage",required = false)MultipartFile file) {
		if(null != file  && !file.isEmpty()) {
			user.setAvatar(postMedia(file));
		}
		return userService.addUser(user);
	}

	@RequestMapping(value = "/updateUser", method = RequestMethod.PUT)
	public User updateUser(User user,@RequestParam(value = "fileImage",required = false)MultipartFile file) {
		if(null != file && !file.isEmpty()) {
			user.setAvatar(postMedia(file));
		}
		return userService.updateUser(user);
	}

	@RequestMapping(value = "/removeUser", method = RequestMethod.DELETE)
	public void deleteUser(@RequestParam(value = "userId", required = true) int userId) {
		userService.deleteUser(new Integer(userId));
	}
	
	@RequestMapping(value="/uploadImage", method = RequestMethod.POST)
	public String uploadImage( MultipartFile fileImage) {
		if(fileImage.isEmpty())return "";
		 try {
	            // Get the file and save it somewhere
	            byte[] bytes = fileImage.getBytes();
	            Path path = Paths.get(UPLOADED_FOLDER + fileImage.getOriginalFilename());
	            Files.write(path, bytes);
	            return path.toString();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		 return"";
		
	}

	@RequestMapping(value = "/listPageable", method = RequestMethod.GET)
	Page<User> employeesPageable(Pageable pageable) {
		return userService.getUserWithPaging(pageable);

	}
	
	
	private String postMedia(MultipartFile file) {
		String fileName = System.currentTimeMillis() + file.getOriginalFilename();

		String filePath = UPLOADED_FOLDER + fileName;
		InputStream inputStream = null;
		OutputStream outputStream = null;

		try {
			Path path = Paths.get(filePath);
			if (Files.notExists(path)) {
				Files.createDirectories(path.getParent());
			}

			inputStream = file.getInputStream();
			outputStream = Files.newOutputStream(path);
			org.apache.tomcat.util.http.fileupload.IOUtils.copy(inputStream, outputStream);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null)
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			if(outputStream != null)
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

		return STATIC_CONTEXT_PATH + fileName;
	}
}
