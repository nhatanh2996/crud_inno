package controller;

import java.awt.Dialog.ModalExclusionType;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

public class UserController {
	
	@GetMapping(value = "/")
	public String homepage() {
		
		return "home";
	}
}
